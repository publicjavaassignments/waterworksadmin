USE [master]
GO
/****** Object:  Database [WaterworkDatabase]    Script Date: 25-10-2019 16:35:07 ******/
CREATE DATABASE [WaterworkDatabase]
 CONTAINMENT = NONE
GO
ALTER DATABASE [WaterworkDatabase] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [WaterworkDatabase].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [WaterworkDatabase] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [WaterworkDatabase] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [WaterworkDatabase] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [WaterworkDatabase] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [WaterworkDatabase] SET ARITHABORT OFF 
GO
ALTER DATABASE [WaterworkDatabase] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [WaterworkDatabase] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [WaterworkDatabase] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [WaterworkDatabase] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [WaterworkDatabase] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [WaterworkDatabase] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [WaterworkDatabase] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [WaterworkDatabase] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [WaterworkDatabase] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [WaterworkDatabase] SET  DISABLE_BROKER 
GO
ALTER DATABASE [WaterworkDatabase] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [WaterworkDatabase] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [WaterworkDatabase] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [WaterworkDatabase] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [WaterworkDatabase] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [WaterworkDatabase] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [WaterworkDatabase] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [WaterworkDatabase] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [WaterworkDatabase] SET  MULTI_USER 
GO
ALTER DATABASE [WaterworkDatabase] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [WaterworkDatabase] SET DB_CHAINING OFF 
GO
ALTER DATABASE [WaterworkDatabase] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [WaterworkDatabase] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [WaterworkDatabase] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [WaterworkDatabase] SET QUERY_STORE = OFF
GO
USE [WaterworkDatabase]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
USE [WaterworkDatabase]
GO
/****** Object:  Table [dbo].[tblCustomer]    Script Date: 25-10-2019 16:35:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCustomer](
	[fldCustomerNumber] [int] IDENTITY(1,1) NOT NULL,
	[fldName] [nchar](50) NULL,
	[fldEmail] [nchar](50) NULL,
	[fldPhoneNumber] [nchar](15) NULL,
	[fldWaterStatus] [bit] NULL,
	[fldSegment] [nchar](15) NULL,
	[fldAddress] [nchar](80) NULL,
	[fldZipcode] [int] NULL,
	[fldCPRNumber] [nchar](15) NULL,
 CONSTRAINT [PK_tblCustomer] PRIMARY KEY CLUSTERED 
(
	[fldCustomerNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblInvoice]    Script Date: 25-10-2019 16:35:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblInvoice](
	[fldInvoiceID] [int] IDENTITY(1,1) NOT NULL,
	[fldCustomerNumber] [int] NULL,
	[fldPricePrCM] [float] NULL,
	[fldStatus] [bit] NULL,
	[fldExpirationDate] [date] NULL,
	[fldPaymentType] [nchar](20) NULL,
	[fldReminderCount] [int] NULL,
	[fldCardID] [int] NULL,
	[fldTotalPrice] [float] NULL,
	[fldTax] [int] NULL,
 CONSTRAINT [PK_tblBill] PRIMARY KEY CLUSTERED 
(
	[fldInvoiceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblReadingCard]    Script Date: 25-10-2019 16:35:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblReadingCard](
	[fldCardID] [int] IDENTITY(1,1) NOT NULL,
	[fldCustomerNumber] [int] NULL,
	[fldConsumption] [float] NULL,
	[fldSentDate] [date] NULL,
 CONSTRAINT [PK_tblReadingCard] PRIMARY KEY CLUSTERED 
(
	[fldCardID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblSegment]    Script Date: 25-10-2019 16:35:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblSegment](
	[fldSegment] [nchar](15) NOT NULL,
	[fldTax] [int] NULL,
	[fldReminderFee] [int] NULL,
	[fldPricePrCM] [float] NULL,
 CONSTRAINT [PK_tblTax] PRIMARY KEY CLUSTERED 
(
	[fldSegment] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblWaterMeter]    Script Date: 25-10-2019 16:35:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblWaterMeter](
	[fldMeterID] [int] NOT NULL,
	[fldCustomerNumber] [int] NULL,
	[fldCardID] [int] NULL,
	[fldMeterAmount] [int] NULL,
 CONSTRAINT [PK_tblWaterMeter] PRIMARY KEY CLUSTERED 
(
	[fldMeterID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCustomer]  WITH CHECK ADD  CONSTRAINT [FK_tblCustomer_tblSegment] FOREIGN KEY([fldSegment])
REFERENCES [dbo].[tblSegment] ([fldSegment])
GO
ALTER TABLE [dbo].[tblCustomer] CHECK CONSTRAINT [FK_tblCustomer_tblSegment]
GO
ALTER TABLE [dbo].[tblInvoice]  WITH CHECK ADD  CONSTRAINT [FK_tblBill_tblCustomer] FOREIGN KEY([fldCustomerNumber])
REFERENCES [dbo].[tblCustomer] ([fldCustomerNumber])
GO
ALTER TABLE [dbo].[tblInvoice] CHECK CONSTRAINT [FK_tblBill_tblCustomer]
GO
ALTER TABLE [dbo].[tblInvoice]  WITH CHECK ADD  CONSTRAINT [FK_tblBill_tblReadingCard1] FOREIGN KEY([fldCardID])
REFERENCES [dbo].[tblReadingCard] ([fldCardID])
GO
ALTER TABLE [dbo].[tblInvoice] CHECK CONSTRAINT [FK_tblBill_tblReadingCard1]
GO
ALTER TABLE [dbo].[tblReadingCard]  WITH CHECK ADD  CONSTRAINT [FK_tblReadingCard_tblCustomer] FOREIGN KEY([fldCustomerNumber])
REFERENCES [dbo].[tblCustomer] ([fldCustomerNumber])
GO
ALTER TABLE [dbo].[tblReadingCard] CHECK CONSTRAINT [FK_tblReadingCard_tblCustomer]
GO
ALTER TABLE [dbo].[tblWaterMeter]  WITH CHECK ADD  CONSTRAINT [FK_tblWaterMeter_tblCustomer] FOREIGN KEY([fldCustomerNumber])
REFERENCES [dbo].[tblCustomer] ([fldCustomerNumber])
GO
ALTER TABLE [dbo].[tblWaterMeter] CHECK CONSTRAINT [FK_tblWaterMeter_tblCustomer]
GO
ALTER TABLE [dbo].[tblWaterMeter]  WITH CHECK ADD  CONSTRAINT [FK_tblWaterMeter_tblReadingCard] FOREIGN KEY([fldCardID])
REFERENCES [dbo].[tblReadingCard] ([fldCardID])
GO
ALTER TABLE [dbo].[tblWaterMeter] CHECK CONSTRAINT [FK_tblWaterMeter_tblReadingCard]
GO
USE [master]
GO
ALTER DATABASE [WaterworkDatabase] SET  READ_WRITE 
GO


USE WaterworkDatabase;
INSERT INTO tblSegment (fldSegment, fldTax, fldReminderFee, fldPricePrCM) VALUES ('Agriculture', 15, 400, 55.46);
INSERT INTO tblSegment (fldSegment, fldTax, fldReminderFee, fldPricePrCM) VALUES ('Industry', 12, 200, 55.46);
INSERT INTO tblSegment (fldSegment, fldTax, fldReminderFee, fldPricePrCM) VALUES ('Private', 10, 100, 55.46);