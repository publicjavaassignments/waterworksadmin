package GUI;

import Functionality.*;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class Controller {

    //Create new customer
    public TextField textFieldCPRNumber;
    public TextField textFieldName;
    public TextField textFieldEmail;
    public TextField textFieldPhoneNumber;
    public TextField textFieldSegment;
    public TextField textFieldAddress;
    public TextField textFieldZipcode;

    public void handleNewCustomer() {
        CreateCustomer customer = new CreateCustomer();
        customer.newCustomer(textFieldCPRNumber, textFieldName, textFieldEmail, textFieldPhoneNumber, textFieldAddress, textFieldZipcode, textFieldSegment);
    }

    //Labels for showing customer info
    public Label labelName;
    public Label labelEmail;
    public Label labelPhoneNumber;
    public Label labelAddress;
    public Label labelZipcode;
    public Label labelSegment;
    public Label labelWaterStatus;
    public Label labelCustomerNumber;
    public TextField textFieldSearchCustomerCN;
    public TextField textFieldSearchCustomerCPR;

    //Search by customer number
    public void handleCustomerCNSearch() {
        ShowCustomer customer = new ShowCustomer();
        customer.getCustomerByCN(textFieldSearchCustomerCN, labelCustomerNumber, labelName, labelEmail, labelPhoneNumber, labelAddress, labelZipcode, labelSegment, labelWaterStatus);

    }

    //Search by CPR
    public void handleCustomerCPRSearch() {
        ShowCustomer customer = new ShowCustomer();
        customer.getCustomerByCPR(textFieldSearchCustomerCPR, labelCustomerNumber, labelName, labelEmail, labelPhoneNumber, labelAddress, labelZipcode, labelSegment, labelWaterStatus);

    }

    //Send new reading card
    public void sendReadingCard() {
        ReadingCard card = new ReadingCard();
        card.sendReadingCard(labelCustomerNumber);
    }

    //Enter consumption from the reading card
    public TextField textFieldCustomerNumber;
    public TextField textFieldConsumptionValue;
    public TextField textFieldCardID;

    public void handleConsumptionInput() {
        Consumption consumption = new Consumption();
        consumption.enterConsumption(textFieldCustomerNumber, textFieldConsumptionValue, textFieldCardID);
    }

    //Create new invoice
    public TextField textFieldCustomerNumberInvoice;
    public TextField textFieldCardIDInvoice;

    public void createInvoice() {
        Invoice invoice = new Invoice();
        invoice.newInvoice(textFieldCustomerNumberInvoice, textFieldCardIDInvoice);
    }

    public TextField textFieldInvoiceNumber;
    public Label labelNameInvoice;
    public Label labelPrice;
    public Label labelTax;
    public Label labelPaymentStatus;
    public Label labelPaymentType;
    public Label labelReminderStatus;
    public Label labelExpirationDate;

    public void showInvoice() {
        Invoice invoice = new Invoice();
        invoice.showInvoice(textFieldInvoiceNumber, labelNameInvoice, labelPrice, labelTax, labelPaymentStatus, labelPaymentType, labelReminderStatus, labelExpirationDate);
    }
}
