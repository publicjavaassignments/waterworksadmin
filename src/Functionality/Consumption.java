package Functionality;

import Database.DB;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;

public class Consumption {

    //Enter consumption from readingcard
    public void enterConsumption(TextField textFieldCustomerNumber, TextField textFieldConsumptionValue, TextField textFieldCardID) {

        try {
            //Update database when customerNumber, CardID and consumptionValue is entered.
            double customerNumber = Double.parseDouble(textFieldCustomerNumber.getText());
            double cardID = Double.parseDouble(textFieldCardID.getText());
            double consumptionValue = Double.parseDouble(textFieldConsumptionValue.getText());
            DB.selectSQL("UPDATE tblReadingCard SET fldConsumption = " + consumptionValue + " where fldCardID = " + cardID + ";");
        } catch (Exception e) {
            //Show alert if the input is wrong
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("ERROR");
            alert.setHeaderText("INPUT NOT ALLOWED");
            alert.setContentText("Please enter correct values");
            alert.showAndWait();
        }

    }

}
