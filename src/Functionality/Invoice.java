package Functionality;

import Database.DB;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import sun.misc.FloatingDecimal;

import java.rmi.server.ExportException;

public class Invoice {

    public void newInvoice(TextField textFieldCustomerNumberInvoice, TextField textFieldCardIDInvoice) {

        try {
            int customerNumber = Integer.parseInt(textFieldCustomerNumberInvoice.getText());
            int cardID = Integer.parseInt(textFieldCardIDInvoice.getText());

            //Get the amount of card ID that belongs to the customer number.
            DB.selectSQL("SELECT fldCardID from tblReadingCard where fldCustomerNumber = " + customerNumber + ";");
            int count = 0;
            while (true) {
                String dbCardID = DB.getData();
                if (dbCardID.equals(DB.NOMOREDATA)) {
                    break;
                } else {
                    count++;
                }
            }

            //Create an array with all the card ID's that belongs to the customer number.
            int[] cardIDList = new int[count];
            DB.selectSQL("SELECT fldCardID from tblReadingCard where fldCustomerNumber = " + customerNumber + ";");
            count = 0;
            while (true) {
                String data = DB.getData();
                if (data.equals(DB.NOMOREDATA)) {
                    break;
                } else {
                    cardIDList[count] = Integer.parseInt(data);
                }
                count++;
            }

            //Get previous card ID from the array.
            int cardIdListLength = cardIDList.length;
            int previousCardID = cardIDList[cardIdListLength - 2];

            //Get previous and new consumption.
            DB.selectSQL("SELECT fldConsumption from tblReadingCard where fldCardID = " + previousCardID + ";");
            double previousConsumption = Double.parseDouble(DB.getData());

            DB.selectSQL("SELECT fldConsumption from tblReadingCard where fldCardID = " + cardID + ";");
            double newConsumption = Double.parseDouble(DB.getData());

            //Get segment
            DB.selectSQL("SELECT fldSegment from tblCustomer where fldCustomerNumber = " + customerNumber + ";");
            String segment = DB.getData();

            //Get tax
            DB.selectSQL("SELECT fldTax from tblSegment where fldSegment = '" + segment + "';");
            double tax = Double.parseDouble(DB.getData());

            //Get price pr m3
            DB.selectSQL("SELECT fldPricePrCM from tblSegment where fldSegment = '" + segment + "';");
            double pricePrCM = Double.parseDouble(DB.getData());

            //Calculate consumption
            double consumption = newConsumption - previousConsumption;

            //Convert consumption in liters to m3
            double literToCM = (consumption / 1000);

            //Calculate total price
            double totalPrice = (literToCM * pricePrCM) * 1 + (tax / 100);

            //Data into database
            DB.selectSQL("INSERT INTO tblInvoice (fldCustomerNumber, fldPricePrCM, fldStatus, fldReminderCount, fldCardID, fldTotalPrice, fldTax) " + "VALUES (" + customerNumber + ", " + pricePrCM + ", " + 0 + ", " + 0 + ", " + cardID + ", " + totalPrice + "," + tax + ");");

        } catch (Exception e) {
            //Show alert if input is not valid.
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("ERROR");
            alert.setHeaderText("INPUT NOT ALLOWED");
            alert.setContentText("Please enter correct values");
            alert.showAndWait();
        }
    }

    //Show the invoice detals in the 'Invoice Details' tab.
    public void showInvoice(TextField textFieldInvoiceNumber, Label labelNameInvoice, Label labelPrice, Label labelTax, Label labelPaymentStatus, Label labelPaymentType, Label labelReminderStatus, Label labelExpirationDate) {

        try {
            int invoiceID = Integer.parseInt(textFieldInvoiceNumber.getText());

            //Get largest invoiceID
            DB.selectSQL("SELECT max(fldInvoiceID) from tblInvoice");
            int largestInvoiceID = Integer.parseInt(DB.getData());

            //Check if invoiceID exist in the database.
            if (invoiceID <= largestInvoiceID) {

                //Get invoice data from tblInvoice and display it on the screen.
                //Get customer Name:
                DB.selectSQL("SELECT fldName from tblCustomer where fldCustomerNumber = (SELECT fldCustomerNumber from tblInvoice where fldInvoiceID = " + invoiceID + ");");
                String invoiceName = DB.getDisplayData();
                labelNameInvoice.setText(invoiceName);

                //Get total price
                DB.selectSQL("SELECT fldTotalPrice from tblInvoice where fldInvoiceId = " + invoiceID + ";");
                String totalPrice = DB.getDisplayData();
                labelPrice.setText(totalPrice + " DKK");

                //Get Tax
                DB.selectSQL("SELECT fldTax from tblInvoice where fldInvoiceID = " + invoiceID + ";");
                String tax = DB.getDisplayData();
                labelTax.setText(tax + "%");

                //Get payment status
                DB.selectSQL("SELECT fldStatus from tblInvoice where fldInvoiceID = " + invoiceID + ";");
                double paymentStatus = Double.parseDouble(DB.getDisplayData());
                if (paymentStatus == 1) {
                    labelPaymentStatus.setText("Paid");
                } else {
                    labelPaymentStatus.setText("Not paid");
                }

                //Get payment type.
                DB.selectSQL("SELECT fldPaymentType from tblInvoice where fldInvoiceID = " + invoiceID + ";");
                String paymentType = DB.getDisplayData();
                labelPaymentType.setText(paymentType);

                //Get amount of reminders sent
                DB.selectSQL("SELECT fldReminderCount from tblInvoice where fldInvoiceID = " + invoiceID + ";");
                String reminderStatus = DB.getDisplayData();
                labelReminderStatus.setText(reminderStatus);

                //Get Expiration date
                DB.selectSQL("SELECT fldExpirationDate from tblInvoice where fldInvoiceID = " + invoiceID + ";");
                String expirationDate = DB.getDisplayData();
                labelExpirationDate.setText(expirationDate);

            } else {
                //Show alert if invoice ID does not exist.
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("ERROR");
                alert.setHeaderText("INPUT NOT ALLOWED");
                alert.setContentText("The invoice ID does not exist");
                alert.showAndWait();
            }
        } catch (Exception e) {
            //Show alert if invoice ID is not a number.
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("ERROR");
            alert.setHeaderText("INPUT NOT ALLOWED");
            alert.setContentText("Please enter an invoice ID");
            alert.showAndWait();
        }

    }

}
