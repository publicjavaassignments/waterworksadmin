package Functionality;

import Database.DB;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;

public class ReadingCard {
    public void sendReadingCard(Label labelCustomerNumber) {
        //Get customer number and create a reading card.
        int customerNumber = Integer.parseInt(labelCustomerNumber.getText());
        DB.selectSQL("Insert into tblReadingCard (fldCustomerNumber, fldSentDate) values ( '" + customerNumber + "', " + "(SELECT CONVERT(date, getdate())));");

        //Show information alert when the readingCard has been send.
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("INFORMATION");
        alert.setHeaderText("Reading card has been sent.");
        alert.showAndWait();
    }
}
