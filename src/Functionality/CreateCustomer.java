package Functionality;

import Database.DB;
import GUI.Controller;
import com.sun.org.apache.xml.internal.security.utils.IgnoreAllErrorHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class CreateCustomer {

    public void newCustomer(TextField textFieldCPRNumber, TextField textFieldName, TextField textFieldEmail, TextField textFieldPhoneNumber, TextField textFieldAddress, TextField textFieldZipcode, TextField textFieldSegment) {

        try {
            //Get values from textfields and insert into tblCustomer
            String CPRNumber = textFieldCPRNumber.getText();
            String customerName = textFieldName.getText();
            String customerEmail = textFieldEmail.getText();
            String customerPhoneNumber = textFieldPhoneNumber.getText();
            String customerAddress = textFieldAddress.getText();
            int customerZipcode = Integer.parseInt(textFieldZipcode.getText());
            String customerSegment = textFieldSegment.getText();

            DB.selectSQL("INSERT INTO tblCustomer (fldCPRNumber, fldName, fldEmail, fldPhoneNumber, fldWaterStatus, fldAddress, fldZipcode, fldSegment) VALUES ('" + CPRNumber + "', '" + customerName + "', '" + customerEmail + "','" + customerPhoneNumber + "'," + 1 + ", '" + customerAddress + "'," + customerZipcode + ",'" + customerSegment + "');");

            //Show alertbox when the customer is created
            Alert newCustomerCreated = new Alert(Alert.AlertType.INFORMATION);
            newCustomerCreated.setTitle("Creation confirmed");
            newCustomerCreated.setHeaderText("The customer has been created!");
            newCustomerCreated.showAndWait();

            //Clear TextFields to be ready for next customer.
            textFieldCPRNumber.clear();
            textFieldName.clear();
            textFieldEmail.clear();
            textFieldPhoneNumber.clear();
            textFieldAddress.clear();
            textFieldZipcode.clear();
            textFieldSegment.clear();
        } catch (Exception e) {

            //Show alert if the input is wrong
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("ERROR");
            alert.setHeaderText("INPUT NOT ALLOWED");
            alert.setContentText("Please enter correct values");
            alert.showAndWait();
        }
    }

}
