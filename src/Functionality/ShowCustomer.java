package Functionality;

import Database.DB;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class ShowCustomer {

    //Search customer by customer NUmber
    public void getCustomerByCN(TextField textFieldSearchCustomerCn, Label labelCustomerNumber, Label labelName, Label labelEmail, Label labelPhoneNumber, Label labelAddress, Label labelZipcode, Label labelSegment, Label labelWaterStatus) {

        try {
            int customerNumber = Integer.parseInt(textFieldSearchCustomerCn.getText());

            //Get highest customer number
            DB.selectSQL("SELECT max(fldCustomerNumber) from tblCustomer;");
            int highestCustomerNumber = Integer.parseInt(DB.getData());

            if (customerNumber <= highestCustomerNumber) {

                //Set customer number label to customer number
                labelCustomerNumber.setText(Integer.toString(customerNumber));

                //Get customer info from the database and add it to label.
                //Get customer name
                DB.selectSQL("SELECT fldName from tblCustomer where fldCustomerNumber = " + customerNumber + ";");
                String customerName = DB.getDisplayData();
                labelName.setText(customerName);

                //Get Email
                DB.selectSQL("SELECT fldEmail from tblCustomer where fldCustomerNumber = " + customerNumber + ";");
                String customerEmail = DB.getDisplayData();
                labelEmail.setText(customerEmail);

                //Get Phone Number
                DB.selectSQL("SELECT fldPhoneNumber from tblCustomer where fldCustomerNumber = " + customerNumber + ";");
                String customerPhoneNumber = DB.getDisplayData();
                labelPhoneNumber.setText(customerPhoneNumber);

                //Get Address
                DB.selectSQL("SELECT fldAddress from tblCustomer where fldCustomerNumber = " + customerNumber + ";");
                String customerAddress = DB.getDisplayData();
                labelAddress.setText(customerAddress);

                //Get Zipcode
                DB.selectSQL("SELECT fldZipcode from tblCustomer where fldCustomerNumber = " + customerNumber + ";");
                String customerZipcode = DB.getDisplayData();
                labelZipcode.setText(customerZipcode);

                //Get segment
                DB.selectSQL("SELECT fldSegment from tblCustomer where fldCustomerNumber = " + customerNumber + ";");
                String customerSegment = DB.getDisplayData();
                labelSegment.setText(customerSegment);

                //Get water status
                DB.selectSQL("SELECT fldWaterStatus from tblCustomer where fldCustomerNumber = " + customerNumber + ";");
                if (Double.parseDouble(DB.getDisplayData()) == 1) {
                    labelWaterStatus.setText("Open");
                } else {
                    labelWaterStatus.setText("Closed");
                }
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("ERROR");
                alert.setHeaderText("INPUT NOW ALLOWED");
                alert.setContentText("Please enter a valid customer number.");
                alert.showAndWait();
            }
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("ERROR");
            alert.setHeaderText("INPUT NOT ALLOWED!");
            alert.setContentText("Please enter a valid customer number.");
            alert.showAndWait();
        }
    }

    //Search customer by CPR number
    public void getCustomerByCPR(TextField textFieldSearchCPR, Label labelCustomerNumber, Label labelName, Label labelEmail, Label labelPhoneNumber, Label labelAddress, Label labelZipcode, Label labelSegment, Label labelWaterStatus) {

        try {
            double cprNumber = Double.parseDouble(textFieldSearchCPR.getText());

            //Get customer info from the database and add it to label.
            //Get customer number
            DB.selectSQL("SELECT fldCustomerNumber from tblCustomer where fldCPRNumber = " + cprNumber + ";");
            int customerNumber = Integer.parseInt(DB.getData());
            labelCustomerNumber.setText("" + customerNumber);

            //Get name
            DB.selectSQL("SELECT fldName from tblCustomer where fldCPRNumber = " + cprNumber + ";");
            String customerName = DB.getDisplayData();
            labelName.setText(customerName);

            //Get Email
            DB.selectSQL("SELECT fldEmail from tblCustomer where fldCPRNumber = " + cprNumber + ";");
            String customerEmail = DB.getDisplayData();
            labelEmail.setText(customerEmail);

            //Get Phone Number
            DB.selectSQL("SELECT fldPhoneNumber from tblCustomer where fldCPRNumber = " + cprNumber + ";");
            String customerPhoneNumber = DB.getDisplayData();
            labelPhoneNumber.setText(customerPhoneNumber);

            //Get Address
            DB.selectSQL("SELECT fldAddress from tblCustomer where fldCPRNumber = " + cprNumber + ";");
            String customerAddress = DB.getDisplayData();
            labelAddress.setText(customerAddress);

            //Get Zipcode
            DB.selectSQL("SELECT fldZipcode from tblCustomer where fldCPRNumber = " + cprNumber + ";");
            String customerZipcode = DB.getDisplayData();
            labelZipcode.setText(customerZipcode);

            //Get Segment
            DB.selectSQL("SELECT fldSegment from tblCustomer where fldCPRNumber = " + cprNumber + ";");
            String customerSegment = DB.getDisplayData();
            labelSegment.setText(customerSegment);

            //Get Water status
            DB.selectSQL("SELECT fldWaterStatus from tblCustomer where fldCPRNumber = " + cprNumber + ";");
            if (Double.parseDouble(DB.getDisplayData()) == 1) {
                labelWaterStatus.setText("Open");
            } else {
                labelWaterStatus.setText("Closed");
            }
        } catch (Exception e) {
            //Show alert if CPR number does not exist
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("ERROR");
            alert.setHeaderText("INPUT NOT ALLOWED!");
            alert.setContentText("Please enter a valid CPR number.");
            alert.showAndWait();
        }
    }

}
